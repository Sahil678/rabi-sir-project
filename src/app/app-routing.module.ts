import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GridComponent } from './grid/grid.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
                        {path:'' , component : LoginComponent},
                        {path:'grid' , component : GridComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
