import { Component, OnInit, ElementRef, ViewChildren, QueryList } from '@angular/core';


@Component({
  selector: 'grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})

export class GridComponent implements OnInit {
      @ViewChildren("desc") Description: QueryList<ElementRef>;
      @ViewChildren("credit") Credit: QueryList<ElementRef>;
      @ViewChildren("debit") Debit: QueryList<ElementRef>;

  private data: Array<any> = [];
  private newRow: Array<any> =[];
  private newAttribute:any = {};
  isDisabled:boolean = true;
  public selectedRow = null;
  showDropDown = false;
  focusDesc = false;
  tempData:any = [];
  sumDebit:number = 0;
  sumCredit:number=0;


  codeTitle = [{accode:'0',title:'Psycho'},{accode:'1',title:'Sahil'},{accode:'2',title:'Saroj'},{accode:'3',title:'Upechya'},{accode:'4',title:'Abdullah'},
  {accode:'5',title:'Pashupati'},{accode:'6',title:'Pashupati1'},{accode:'7',title:'Pashupati2'},{accode:'8',title:'Pashupati3'},{accode:'9',title:'Pashupati4'},
  {accode:'10',title:'Pashupati5'},{accode:'11',title:'Pashupati6'},{accode:'12',title:'Pashupati7'}]



  constructor() {
    this.tempData = this.codeTitle;
   }

  ngOnInit() {
    this.addNewRow();
}


  addNewRow(){
    this.isDisabled = true;
    this.data.push(this.newAttribute);
    this.newRow.push(this.newAttribute);
    this.newAttribute = {};
  }

  toggleDropdown(selectedRow){
    this.selectedRow = selectedRow;
    this.showDropDown = !this.showDropDown;
  }


  selectValue(value){

   this.newRow[this.selectedRow] = {accode : value.accode, title: value.title};
   this.showDropDown = false;
   //this.Description.toArray()[this.selectedRow].nativeElement.focus();
  }


disableDebit(event){
 var value = event.target.value;
 if(value === "0"){
     this.isDisabled = false;
  }else{
    this.isDisabled = true;
  }
}

searchData(event){
  var searchParams = event.target.value;
  const val = searchParams === undefined ? '' :  searchParams.toLowerCase();
  const temp = this.tempData.filter((d)=>{
    if(d.title !== undefined){
      return d.title.toLocaleLowerCase().indexOf(val) !== -1 || d.accode.indexOf(val) !== -1  || !val
    }
  });
  this.codeTitle = temp;
}

creditSum(event){
   var tempcredit = Number(event.target.value);
  this.sumCredit += tempcredit;
}

debitSum(event){
  var tempdebit = Number(event.target.value);
  this.sumDebit += tempdebit;
}

deleteRow(selectedRow){
  var credit = this.Credit.toArray()[selectedRow].nativeElement.value;
  var debit = this.Debit.toArray()[selectedRow].nativeElement.value;
  this.sumCredit -= credit;
  this.sumDebit -= debit;
  this.newRow.splice(selectedRow,1);
}
}
