import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public authenticationFlag: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  onSubmit(login){
    var username = login.username;
    var password = login.password;
    console.log(username, password);
  }

}
